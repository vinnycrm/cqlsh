FROM python:3.8-buster
RUN python3 -m venv /opt/venv

WORKDIR /scripts

ENV CQLVERSION="6.0.0" \
    CQLSH_HOST="cassandra" \
    CQLSH_PORT="9042"

RUN pip install -U cqlsh \
    && mkdir /.cassandra

COPY ["entrypoint.sh", "/usr/local/bin"]

USER nobody

CMD ["entrypoint.sh"]